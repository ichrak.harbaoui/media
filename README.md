## MediaSI
 Our platform aims to stop the spread of false news with a dedicated voting system which will allow the users to decide whether to keep the post or take it off the platform if found to be fake. This will give the news shared on our website more credibility and give more control over the spread of false news.

<h1 align="center">
🌐 MERN Stack
</h1>
<p align="center">
MongoDB, Expressjs, React/Redux, Nodejs
</p>


> MERN is a fullstack implementation in MongoDB, Expressjs, React/Redux, Nodejs.

MERN stack is the idea of using Javascript/Node for fullstack web development.

## clone or download
```terminal
$ git clone https://gitlab.com/ichrak.harbaoui/media
```

## project structure
```terminal
package.json
server/
   package.json
   .env (to create .env, check [prepare your secret session])
app/
   package.json
...
```

# Usage (run fullstack app on your machine)

## Prerequirements
- [MongoDB](https://gist.github.com/nrollr/9f523ae17ecdbb50311980503409aeb3)
- [Node](https://nodejs.org/en/download/) 
- [npm](https://nodejs.org/en/download/package-manager/)

notice, you need client and server runs concurrently in different terminal session, in order to make them talk to each other

## App-side usage(PORT: 3000)
```terminal
$ cd app   // go to client folder
$ npm i       // npm install packages
$ npm start // run it locally

// deployment for client app
$ npm run build // this will compile the react code using webpack and generate a folder called docs in the root level
$ npm run start // this will run the files in docs, this behavior is exactly the same how gh-pages will run your static site
```

## Server-side usage(PORT: 5001)

### Start

```terminal
$ cd server   // go to server folder
$ npm i       // npm install pacakges
$ npm run dev // run it locally
$ npm run build // this will build the server code to es5 js codes and generate a dist file
```

# Dependencies(tech-stacks)
| Server-side
****************

bcryptjs,
mongoose,
multer,
socket.io,
nodemailer,
@hapi/joi (Validation),

************
| Client-side 

jwt-decode,
momentjs,
react,
react-dom,
react-hook-form,
react-redux,
react-router-dom,
redux-thunk,
socket.io-client,
axios,
***********
# Exemple of API

| Name | API |   Params   |
| ------ | ------ | ------ |
| Register | [http://localhost:5001/api/auth/register] |  name,email,phone,dateOfBirth,password,imgUrl |
| Login | [http://localhost:5001/api/auth/login] | email , password |
| Chat | [http://localhost:5001/api/chat/] | fromID, toID|
| Update Password | [http://localhost:5001/api/auth/updatePassword/] | userID,password |
| Upload image | [http://localhost:5001/upload] | formData |
| Delete Message | [http://localhost:5001/api/chat/deleteMsg/] | msgID |
| Delete Notification | [http://localhost:5001/api/chat/deleteNotif] | fromID , toID |
| Create Post | [http://localhost:5001/api/posts] | content,type,mediaURL|
| Fake Post | [http://localhost:5001/api/posts/fakePost/] | postID |
| Add Comment | [http://localhost:5001/api/comments/create/] | postID ,  content |
| Like Post | [http://localhost:5001/api/posts/like/] | postID|
| Search user | [http://localhost:5001/api/users/search/] | name |
| Send Friend Request | [http://localhost:5001/api/users/friendrequest/] | userID |



# Screenshots of this project
User can sign in or sign up
![User can sign in or sign up](https://i.imgur.com/7eOfGBq.png)
![](https://i.imgur.com/8FhoPgB.png)


you can visit Home page
![User visit Home page](https://i.imgur.com/J4p6IP3.png)



* View your friends' profil
![ View your friends' profil](https://i.imgur.com/MpTtggN.png)

* Voting system for fake news
![ Voting system for fake news](https://i.imgur.com/AgDpzu2.png)

## BUGs or comments

[Create new Issues](https://gitlab.com/ichrak.harbaoui/media/-/issues) (preferred)

## Author
[developer-team](https://gitlab.com/ichrak.harbaoui/media)