const router = require('express').Router();

const Chat = require('../model/Chat')
const User = require('../model/User')

// Gets last 20 chat messages between 2 user
router.get('/:fromID/:toID', async (req, res) => {
    const criteria = { $or: [{ from: req.params.fromID, to: req.params.toID }, { from: req.params.toID, to: req.params.fromID }] }
    try {
        await Chat.find(criteria).sort({ "date": -1 }).limit(20).exec((err, doc) => {
            res.json(doc)
        })
    } catch (error) {

    }
})

router.get('/deleteNotif', async (req, res) => {
    const { from,to } = req.query;
    try {
     await User.findById(to).updateOne({ $pull: { notifications: from } });
     res.json({
        "status": "success"
    })
    } catch (error) {
    console.log(error);
    }
})

router.delete('/deleteMsg/:msgID', async (req, res) => {
    await Chat.findOneAndDelete({ _id: req.params.msgID} ).exec(function (err, result) {
      if (err) res.send(err)
      else res.send(result)
    })
  })

module.exports = router;